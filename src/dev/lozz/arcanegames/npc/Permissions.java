package dev.lozz.arcanegames.npc;

public class Permissions {

    public static final String NPC_COMMAND = "arcanegames.npc.command";
    public static final String NPC_COMMAND_CREATE = "arcanegames.npc.command.create";
    public static final String NPC_COMMAND_DELETE = "arcanegames.npc.command.delete";
    public static final String NPC_COMMAND_LIST = "arcanegames.npc.command.list";
    public static final String NPC_COMMAND_MOVE = "arcanegames.npc.command.move";
}
