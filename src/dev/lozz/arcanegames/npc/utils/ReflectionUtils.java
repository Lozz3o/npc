package dev.lozz.arcanegames.npc.utils;

import java.lang.reflect.Field;

public class ReflectionUtils {

    public static Object getField(String name, Class clazz, Object key) {
        try {
            Field field = clazz.getDeclaredField(name);

            if (!field.isAccessible()) {
                field.setAccessible(true);
            }

            return field.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
