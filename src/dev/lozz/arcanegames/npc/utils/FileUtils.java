package dev.lozz.arcanegames.npc.utils;

import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {

    public static void createFile(File file) {
        try {
            file.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void saveFile(FileConfiguration config, File file) {
        try {
            config.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void deleteFile(File file) {
        try {
            Files.delete(Paths.get(file.getPath()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
