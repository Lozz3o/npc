package dev.lozz.arcanegames.npc.utils;

import org.bukkit.ChatColor;

public class ColorUtils {

    public static String translateMessage(String message) {
        return ChatColor.translateAlternateColorCodes('&', message);
    }
}
