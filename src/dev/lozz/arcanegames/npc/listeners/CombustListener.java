package dev.lozz.arcanegames.npc.listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;

public class CombustListener implements Listener {

    @EventHandler
    public void onCombust(EntityCombustEvent event) {
        Entity entity = event.getEntity();

        if (entity instanceof Player) return;
        if (!entity.hasMetadata("ArcaneNPC")) return;

        event.setCancelled(true);
    }
}
