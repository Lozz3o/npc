package dev.lozz.arcanegames.npc.listeners;

import dev.lozz.arcanegames.npc.npc.NPC;
import dev.lozz.arcanegames.npc.npc.events.NPCInteractEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class InteractListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEntityEvent event) {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();

        if (entity instanceof Player) return;
        if (!entity.hasMetadata("ArcaneNPC")) return;

        NPC npc = (NPC) entity.getMetadata("ArcaneNPC").iterator().next().value();
        NPCInteractEvent e = new NPCInteractEvent(player, npc);

        event.setCancelled(true);

        Bukkit.getPluginManager().callEvent(e);
    }
}
