package dev.lozz.arcanegames.npc.npc.events;

import dev.lozz.arcanegames.npc.npc.NPC;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class NPCInteractEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final Player player;
    private final NPC npc;

    public NPCInteractEvent(Player player, NPC npc) {
        this.player = player;
        this.npc = npc;
    }
    public Player getPlayer() {
        return player;
    }
    public NPC getNPC() {
        return npc;
    }
    public HandlerList getHandlers() {
        return handlers;
    }
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
