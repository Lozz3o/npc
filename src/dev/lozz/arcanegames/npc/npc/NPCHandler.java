package dev.lozz.arcanegames.npc.npc;

import dev.lozz.arcanegames.npc.utils.ConfigurationUtils;
import dev.lozz.arcanegames.npc.utils.FileUtils;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class NPCHandler {

    private final Set<NPC> npc;

    public NPCHandler() {
        npc = new HashSet<>();

        loadNPC();
    }
    private void loadNPC() {
        File directory = new File("plugins" + File.separator + "ArcaneNPC" + File.separator + "NPC");

        directory.mkdirs();

        for (File file : directory.listFiles()) {
            FileConfiguration config = YamlConfiguration.loadConfiguration(file);

            Location location = ConfigurationUtils.deserializeLocation(config, "Location");
            EntityType type = getTypeByName(config.getString("Type"));

            NPC npc = new NPC(file.getName(), location, type);

            npc.spawn();

            this.npc.add(npc);
        }
    }
    private void saveNPC() {
        File directory = new File("plugins" + File.separator + "ArcaneNPC" + File.separator + "NPC");

        directory.mkdirs();

        for (NPC npc : npc) {
            File file = new File(directory, npc.getName());

            if (!file.exists()) {
                FileUtils.createFile(file);
            }

            FileConfiguration config = YamlConfiguration.loadConfiguration(file);

            ConfigurationUtils.serializeLocation(config, npc.getLocation(), "Location");
            config.set("Type", npc.getType().name());

            FileUtils.saveFile(config, file);

            npc.despawn();
        }
    }
    public void save() {
        saveNPC();
    }
    public EntityType getTypeByName(String type) {
        for (EntityType entityType : EntityType.values()) {
            if (!entityType.name().equalsIgnoreCase(type)) continue;

            return entityType;
        }
        return null;
    }
    public void createNPC(Location location, EntityType type, String name) {
        NPC npc = new NPC(name, location, type);

        npc.spawn();

        this.npc.add(npc);
    }
    public void deleteNPC(NPC npc) {
        npc.despawn();

        this.npc.remove(npc);

        deleteFileIfExists(npc);
    }
    private void deleteFileIfExists(NPC npc) {
        File directory = new File("plugins" + File.separator + "ArcaneNPC" + File.separator + "NPC");

        File file = new File(directory, npc.getName());

        if (!file.exists()) return;

        FileUtils.deleteFile(file);
    }
    public void teleport(NPC npc, Location location) {
        npc.teleport(location);
    }
    public NPC getNPCByName(String name) {
        for (NPC npc : npc) {
            if (!npc.getName().equalsIgnoreCase(name)) continue;

            return npc;
        }
        return null;
    }
    public List<NPC> getNPC() {
        return npc.stream().collect(Collectors.toList());
    }
}
