package dev.lozz.arcanegames.npc.npc;

import dev.lozz.arcanegames.npc.ArcaneNPC;
import dev.lozz.arcanegames.npc.utils.ReflectionUtils;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.List;

public class NPC {

    private final String name;
    private final EntityType type;

    private Location location;

    private EntityInsentient entity;
    private Entity bukkitEntity;

    protected NPC(String name, Location location, EntityType type) {
        this.name = name;
        this.location = location;
        this.type = type;
    }
    public String getName() {
        return name;
    }
    public Location getLocation() {
        return location;
    }
    public EntityType getType() {
         return type;
    }
    protected void spawn() {
        if (!location.getChunk().isLoaded()) {
            location.getChunk().load();
        }
        //spawn entity & set direction
        bukkitEntity = location.getWorld().spawnEntity(location, type);

        entity = (EntityInsentient) ((CraftEntity) bukkitEntity).getHandle();
        entity.setPositionRotation(location.getX(), location.getY(), location.getZ(), location.getYaw(), location.getPitch());

        //Set NoAI to true
        NBTTagCompound tag = new NBTTagCompound();

        entity.c(tag);
        tag.setInt("NoAI", 1);
        entity.f(tag);

        //Modify pathfinder goals
        PathfinderGoalSelector goal = entity.goalSelector;
        PathfinderGoalSelector target = entity.targetSelector;

        List goalB = (List) ReflectionUtils.getField("b", PathfinderGoalSelector.class, goal);
        List goalC = (List) ReflectionUtils.getField("c", PathfinderGoalSelector.class, goal);
        List targetB = (List) ReflectionUtils.getField("b", PathfinderGoalSelector.class, target);
        List targetC = (List) ReflectionUtils.getField("c", PathfinderGoalSelector.class, target);

        goalB.clear();
        goalC.clear();
        targetB.clear();
        targetC.clear();

        //Set NPC metadata
        bukkitEntity.setMetadata("ArcaneNPC", new FixedMetadataValue(ArcaneNPC.getInstance(), this));
    }
    protected void despawn() {
        entity.getBukkitEntity().remove();
    }
    public void teleport(Location location) {
        despawn();

        this.location = location;

        spawn();
    }
    public Entity getBukkitEntity() {
        return bukkitEntity;
    }
    public EntityInsentient getEntityInsentient() {
        return entity;
    }
}
