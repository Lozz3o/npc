package dev.lozz.arcanegames.npc;

import dev.lozz.arcanegames.npc.commands.NPCCommand;
import dev.lozz.arcanegames.npc.listeners.CombustListener;
import dev.lozz.arcanegames.npc.listeners.DamageListener;
import dev.lozz.arcanegames.npc.listeners.InteractListener;
import dev.lozz.arcanegames.npc.npc.NPCHandler;
import dev.lozz.arcanegames.npc.utils.CommandUtils;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ArcaneNPC extends JavaPlugin {

    private static ArcaneNPC instance;

    private NPCHandler npcHandler;

    @Override
    public void onEnable() {
        saveDefaultConfig();

        instance = this;

        npcHandler = new NPCHandler();

        Bukkit.getPluginManager().registerEvents(new CombustListener(), this);
        Bukkit.getPluginManager().registerEvents(new DamageListener(), this);
        Bukkit.getPluginManager().registerEvents(new InteractListener(), this);
        CommandUtils.registerCommand(new NPCCommand());
    }
    @Override
    public void onDisable() {
        npcHandler.save();
    }
    public static ArcaneNPC getInstance() {
        return instance;
    }
    public NPCHandler getNpcHandler() {
        return npcHandler;
    }
}
