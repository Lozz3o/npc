package dev.lozz.arcanegames.npc.commands;

import dev.lozz.arcanegames.npc.ArcaneNPC;
import dev.lozz.arcanegames.npc.Permissions;
import dev.lozz.arcanegames.npc.npc.NPC;
import dev.lozz.arcanegames.npc.npc.NPCHandler;
import dev.lozz.arcanegames.npc.utils.CollectionUtils;
import dev.lozz.arcanegames.npc.utils.ColorUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class NPCCommand extends BukkitCommand {

    private final FileConfiguration config;
    private final String usage;
    private final NPCHandler npcHandler;

    public NPCCommand() {
        super ("NPC");

        config = ArcaneNPC.getInstance().getConfig();

        usage = ColorUtils.translateMessage(CollectionUtils.combineCollection(config.getStringList("CommandUsage")).replace("{COMMAND}", getName()));
        npcHandler = ArcaneNPC.getInstance().getNpcHandler();
    }
    @Override
    public boolean execute(CommandSender sender, String label, String[] args) {
        if (!sender.hasPermission(Permissions.NPC_COMMAND)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length < 2) {
            sender.sendMessage(usage);
            return false;
        }
        if (args[0].equalsIgnoreCase("create")) {

            return create(sender, args);

        } else if (args[0].equalsIgnoreCase("delete")) {

            return delete(sender, args);

        } else if (args[0].equalsIgnoreCase("list")) {

            return list(sender, args);

        } else if (args[0].equalsIgnoreCase("move")) {

            return move(sender, args);

        } else {
            sender.sendMessage(usage);
            return false;
        }
    }
    private boolean create(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("OnlyPlayers")));
            return false;
        }
        if (!sender.hasPermission(Permissions.NPC_COMMAND_CREATE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length < 3 || args.length > 3) {
            sender.sendMessage(usage);
            return false;
        }
        NPC npc = npcHandler.getNPCByName(args[1]);

        if (npc != null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NPCAlreadyExists")));
            return false;
        }
        EntityType type = npcHandler.getTypeByName(args[2]);

        if (type == null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("InvalidType")));
            return false;
        }
        npcHandler.createNPC(((Player) sender).getLocation(), type, args[1]);
        sender.sendMessage(ColorUtils.translateMessage(config.getString("NPCCreated")));

        return true;
    }
    private boolean delete(CommandSender sender, String[] args) {
        if (!sender.hasPermission(Permissions.NPC_COMMAND_DELETE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length > 2) {
            sender.sendMessage(usage);
            return false;
        }
        NPC npc = npcHandler.getNPCByName(args[1]);

        if (npc == null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NPCDoesNotExist")));
            return false;
        }

        npcHandler.deleteNPC(npc);
        sender.sendMessage(ColorUtils.translateMessage(config.getString("NPCDeleted").replace("{NAME}", npc.getName())));

        return true;
    }
    private boolean list(CommandSender sender, String[] args) {
        if (!sender.hasPermission(Permissions.NPC_COMMAND_LIST)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length > 2) {
            sender.sendMessage(usage);
            return false;
        }
        int page = 0;

        try {
            page = Integer.parseInt(args[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (page <= 0) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("InvalidPage")));
            return false;
        }
        List<NPC> npc = npcHandler.getNPC();
        List<NPC> paginated = CollectionUtils.paginateList(npc, page, 5);

        if (paginated.isEmpty()) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("InvalidPage")));
            return false;
        }
        String format = ColorUtils.translateMessage(config.getString("NPCListFormat"));

        List<String> toString = paginated.stream().map(element -> format.replace("{NPC}", element.getName())).collect(Collectors.toList());
        String combined = CollectionUtils.combineCollection(toString);

        List<String> npcList = config.getStringList("NPCList");
        String s = ColorUtils.translateMessage(CollectionUtils.combineCollection(npcList)).replace("{NPC}", combined);

        sender.sendMessage(s.replace("{PAGE}", args[1]));

        return true;
    }
    private boolean move(CommandSender sender, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("OnlyPlayers")));
            return false;
        }
        if (!sender.hasPermission(Permissions.NPC_COMMAND_MOVE)) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NoPermission")));
            return false;
        }
        if (args.length > 2) {
            sender.sendMessage(usage);
            return false;
        }
        NPC npc = npcHandler.getNPCByName(args[1]);

        if (npc == null) {
            sender.sendMessage(ColorUtils.translateMessage(config.getString("NPCDoesNotExist")));
            return false;
        }

        npcHandler.teleport(npc, ((Player) sender).getLocation());
        sender.sendMessage(ColorUtils.translateMessage(config.getString("NPCMoved").replace("{NAME}", npc.getName())));

        return true;
    }
}
